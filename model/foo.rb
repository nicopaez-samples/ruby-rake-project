#require 'byebug'

class Foo

  def do_foo
    #byebug # this is an example to show how to invoke the debugger
    'foo'
  end

end
